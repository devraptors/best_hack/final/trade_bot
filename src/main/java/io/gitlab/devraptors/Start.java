package io.gitlab.devraptors;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import netscape.javascript.JSObject;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;


class JThread extends Thread {
    String name;
    long ID;
    String start_time;
    String end_time;

    JThread(long ID, String name, String start_time, String end_time){
        this.name = name;
        this.ID = ID;
        this.start_time = start_time;
        this.end_time = end_time;
    }

    public void run(){

        System.out.printf("%s started... \n", Thread.currentThread().getName());

        try {
            trading_simulator();
        } catch (Exception e) {
            e.printStackTrace();
        }

        System.out.printf("%s finished... \n", Thread.currentThread().getName());
    }

    public void trading_simulator() throws Exception {
        int i = 0;
        boolean k = false;
        long start_time_mills = getMilesTime(start_time);
        long end_time_mills = getMilesTime(end_time);
        JSONParser jsonParser = new JSONParser();
        ArrayList<String> last_list = new ArrayList<>();
        ArrayList<String> time_list = new ArrayList<>();
        JSONObject jsonObject = null;
        while(true){
            try {
                jsonObject = (JSONObject) jsonParser.parse(sendGetJson_GetInfoByName("http://localhost:8080/api/tool/", name, i));
                if ((getMilesTime(jsonObject.get("time").toString()) >= start_time_mills) && (getMilesTime(jsonObject.get("time").toString()) <= end_time_mills)) {
                    k = true;
                    last_list.add(jsonObject.get("last").toString());
                    time_list.add(jsonObject.get("time").toString());
                    i++;
                } else {
                    if (k == true) {
                        break;
                    }
                    i++;
                }
            }catch(Exception e){
                break;
            }

        }

        System.out.println("We've exited from while: "+last_list);

        ArrayList<Integer> list_U = new ArrayList<>();
        ArrayList<Integer> list_D = new ArrayList<>();

        for (int j = 1; j < last_list.size(); j++){
            if(Double.parseDouble(last_list.get(j)) > Double.parseDouble(last_list.get(j-1))){
                list_U.add((int)(Double.parseDouble(last_list.get(j))-Double.parseDouble(last_list.get(j-1))));
                list_D.add(0);
            }else if(Double.parseDouble(last_list.get(j)) < Double.parseDouble(last_list.get(j-1))){
                list_D.add((int)(Double.parseDouble(last_list.get(j-1))-Double.parseDouble(last_list.get(j))));
                list_U.add(0);
            }else{
                list_D.add(0);
                list_U.add(0);
            }
        }

        System.out.println("LIST_D: "+list_D);
        System.out.println("LIST_U: "+list_U);

        ArrayList<Double> list_D_mod = new ArrayList<>();
        ArrayList<Double> list_U_mod = new ArrayList<>();
        double a = (2.0)/(15.0);

        list_D_mod.add((double)(list_D.get(0)));
        list_U_mod.add((double)(list_U.get(0)));

        for(int t = 1; t < list_D.size(); t++){
            double sum_d = ((double)list_D.get(t)*a)+((1-a)*((double)list_D_mod.get(t-1)));
            list_D_mod.add(sum_d);
        }

        for(int d = 1; d < list_U.size(); d++){
            double sum_u = ((double)list_U.get(d)*a)+((1-a)*((double)list_U_mod.get(d-1)));
            list_U_mod.add(sum_u);
        }

        System.out.println("LIST_D_MOD: "+list_D_mod);
        System.out.println("LIST_U_MOD: "+list_U_mod);

        ArrayList<Double> list_RS = new ArrayList<>();

        for(int l = 0; l < list_D_mod.size(); l++){
            if(list_D_mod.get(l)==0){
                list_RS.add(5000.0);
            }else{
                list_RS.add((list_U_mod.get(l))/(list_D_mod.get(l)));
            }
        }

        System.out.println("LIST_RS: "+list_RS);

        ArrayList<Double> list_RSI = new ArrayList<>();

        for (int m = 0; m < list_RS.size(); m++){
            if(list_RS.get(m)==5000.0){
                list_RSI.add(100.0);
            }else {
                list_RSI.add(100.0 - ((100.0) / (1.0 + list_RS.get(m))));
            }
        }

        System.out.println("LIST_RSI: "+list_RSI);

        ArrayList<JSONObject> transactions_list = new ArrayList<>();

        double balance = 1000000.0;
        int amount = 0;

        for (int tick = 0; tick < list_RSI.size(); tick++){

            if(list_RSI.get(tick) >= 70.0){
                if(amount>0){
                    balance = balance + (double)(amount*Double.parseDouble(last_list.get(tick)));
                    amount = 0;
                    JSONObject transaction = new JSONObject();
                    transaction.put("timestamp",time_list.get(tick));
                    transaction.put("direction", "sell");
                    transaction.put("price", last_list.get(tick));
                    transaction.put("volume", (amount*Double.parseDouble(last_list.get(tick))));
                    transaction.put("amount", amount);
                    transaction.put("balance", balance);
                    transactions_list.add(transaction);
                }
            }
            if(list_RSI.get(tick) <= 30.0){
                int can_buy = (int)((balance)/(Double.parseDouble(last_list.get(tick))));
                if((balance>(double)(can_buy*Double.parseDouble(last_list.get(tick))))&&(can_buy>0)){
                    balance = balance - (double)(can_buy*Double.parseDouble(last_list.get(tick)));
                    amount = amount + can_buy;
                    JSONObject transaction = new JSONObject();
                    transaction.put("timestamp",time_list.get(tick));
                    transaction.put("direction", "buy");
                    transaction.put("price", last_list.get(tick));
                    transaction.put("volume", (amount*Double.parseDouble(last_list.get(tick))));
                    transaction.put("amount", amount);
                    transaction.put("balance", balance);
                    transactions_list.add(transaction);
                }
            }

        }

        System.out.println("Trans list: "+transactions_list);


        //String json_line = sendGet("localhost:8000/csv?tool=nvidia&index=1");
        //String json_line = "{'10:44:20' : '18579.000000000'}";
        //System.out.println(sendGet("http://localhost:8080/api/tools"));

        //sendGetJson_GetInfoByName("http://localhost:8080/api/tool/", "AMD-RM", 1);


    }



    private String sendGet(String req) throws Exception {
        HttpGet request = new HttpGet(req);
        CloseableHttpClient httpCli = HttpClients.createDefault();
        try (CloseableHttpResponse response = httpCli.execute(request);) {
            HttpEntity entity = response.getEntity();
            if (entity != null) {
                String result = EntityUtils.toString(entity);
                return result;
            }
        }
        return null;
    }

    private String sendGetJson_GetInfoByName (String req, String tool, Integer index) throws IOException {
        URL obj = new URL(req);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();
        con.setRequestMethod("POST");
        con.setRequestProperty("Content-Type", "application/json; utf-8");
        con.setRequestProperty("Accept", "application/json");

        con.setDoOutput(true);
        JSONObject json = new JSONObject();
        json.put("tool", tool.toString());
        json.put("index", index);
        System.out.println("JSON: "+json);

        OutputStream os = con.getOutputStream();
        os.write(json.toString().getBytes("UTF-8"));
        os.close();

        InputStream request = con.getInputStream();
        JSONObject data = parseJsonFromRequest(request);
        System.out.println("toJSONString: "+data.toJSONString());
        return data.toJSONString();
    }


    public long getMilesTime(String time){
        DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss", new Locale("en"));
        Date time_date = new Date();
        try
        {
            //преобразуем String в Date
            time_date = dateFormat.parse(time);
            //получаем миллисекунды из даты
            long time_mils=time_date.getTime();
            return time_mils;
        } catch (ParseException e)
        {
            System.out.print("Error of time parsing!");
        }
        return -1;
    }

    public static JSONObject parseJsonFromRequest(InputStream stream) throws IOException {
        JSONParser jsonParser = new JSONParser();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject = (JSONObject)jsonParser.parse(new InputStreamReader(stream));
        } catch (UnsupportedEncodingException | org.json.simple.parser.ParseException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

}



public class Start {

    public static void main(String[] args) {



        String[] tools = {"MSFT-RM", "MOEX.CSCO-RM:FQBR"};
        String start_time = "11:10:10";
        String end_time = "18:30:20";
        int tools_len = tools.length;
        //Запускаем симуляции
        for (int i = 0; i < tools_len; i++){
            trade_sim(89123123, tools[i], start_time, end_time);
        }

    }

    public static void trade_sim(long ID, String tool, String start_time, String end_time){

        //System.out.println("Main thread started...");
        new JThread(ID, tool, start_time, end_time).start();
        //System.out.println("Main thread finished...");

    }

}
